//
//  viewModel.swift
//  SwiftyLibExamples
//
//  Created by TAS on 14/6/21.
//

import Foundation
import SwiftLibTas

final class ViewModel {
    func executeAPI() {
        
        let swiftLibTas: SwiftLibTas = SwiftLibTas()
        
        //swiftLibTas.getJSON(url: "http://tasiberiacordoba.dyndns.org:8180/TAS-CIS/login")
        
        //print("Desde interfaz:  \(swiftLibTas.newGetUserData(url: "http://tasiberiacordoba.dyndns.org:8180/TAS-CIS/login"))")
        //print("Res interfaz: \(swiftLibTas.getJSON())")
        //print("Res old interfaz: \(swiftLibTas.oldGetJSON())")
        
        let body = ["usrName": "admin", "usrPasswd": "admin"]
        let headers = ["": ""]
        let certPath = Bundle.main.url(forResource: "certificatestas", withExtension: "cer")!
        let dataCertPath = try! Data(contentsOf: certPath)
        
        swiftLibTas.getToken(url: "https://tasiberiacordoba.dyndns.org:8180/TAS-CIS", method: "POST", headers: headers, body: body, dataCertPath: dataCertPath, sslOrNot: true, completion: { response in
            print(response)
        })
    }
}

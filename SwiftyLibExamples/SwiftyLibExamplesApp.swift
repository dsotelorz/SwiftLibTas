//
//  SwiftyLibExamplesApp.swift
//  SwiftyLibExamples
//
//  Created by TAS on 10/6/21.
//

import SwiftUI

@main
struct SwiftyLibExamplesApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}

//
//  ContentView.swift
//  SwiftyLibExamples
//
//  Created by TAS on 10/6/21.
//

import SwiftUI

struct ContentView: View {
    
    private var viewModel: ViewModel = ViewModel()
    
    var body: some View {
        VStack {
            Text("Pulsame").padding()
            Button("AQUI") {
                viewModel.executeAPI()
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

//
//  TasCertificateManager.swift
//  SwiftLibTas
//
//  Created by TAS on 30/6/21.
//

import Foundation
import Security

public final class TasCertificatesManager:NSObject {
    
    private var session: URLSession!
    
    public override init() {}
    
    var certificates: [Data] = []
    var urlTas: String = "NoURL"
    
    public func getSessionTas(dataCertPath: [Data], url: String) -> URLSession {
        
        certificates = dataCertPath
        urlTas = url
        
        session = URLSession(configuration: .ephemeral, delegate: self, delegateQueue: nil)
        
        return session
    
    }
}

extension TasCertificatesManager: URLSessionDelegate {
    
    public func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Swift.Void) {
             
             if (challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodClientCertificate) {
                 completionHandler(.rejectProtectionSpace, nil)
             }
         
             if (challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust) {
                 if let serverTrust = challenge.protectionSpace.serverTrust {
                     
                     var secresult = SecTrustResultType.invalid
                     let status = SecTrustEvaluate(serverTrust, &secresult)
                     
                     let protocols = ["TLSv1.2"]
                     
                     let url = URL(string: "wss://"+urlTas)!
                    session.webSocketTask(with: url, protocols: protocols)
                     

                     if errSecSuccess == status {
                         if let serverCertificate = SecTrustGetCertificateAtIndex(serverTrust, 0) {
                             let serverCertificateData = SecCertificateCopyData(serverCertificate) as Data
                             if certificates.contains(serverCertificateData) {
                                     completionHandler(.useCredential, URLCredential(trust: serverTrust))
                                     return
                             }
                         }
                     }
                 }
             }

             // Pinning failed
             completionHandler(URLSession.AuthChallengeDisposition.cancelAuthenticationChallenge, nil)
     }
    
}

//
//  SwiftLibTas.swift
//  SwiftLibTas
//
//  Created by TAS on 10/6/21.
//
import Foundation
import Security

public final class SwiftLibTas:NSObject {
    
    private var session: URLSession!
    
    public override init() {}

    let name = "SwiftLibTas"
    
    // Get Token
    public func getToken(url: String, method: String, headers:Dictionary<String, String>, body:Dictionary<String, String>, dataCertPath: Data, sslOrNot: Bool, completion: @escaping (Dictionary<String, Any>) -> ()) {
        
        let urlPath = "/login"
        let urlURL = URL(string: url+urlPath)!
        var request = URLRequest(url: urlURL)
        request.httpMethod = method
        request.setValue("application/json", forHTTPHeaderField: "content-type")
        
        headers.forEach {(key: String, value: String) in
            request.setValue(value, forHTTPHeaderField: key)
        }
        
        let encoder = JSONEncoder()
        if let jsonData = try? encoder.encode(body) {
            if let jsonString = String(data: jsonData, encoding: .utf8) {
                request.httpBody = jsonString.data(using: .utf8)
            }
        }
        
        if(sslOrNot) {
            let portToString = String(urlURL.port!)
            let urlTAS = urlURL.host!+":"+portToString
            session = TasCertificatesManager().getSessionTas(dataCertPath: [dataCertPath], url: urlTAS)
        } else {
            session = URLSession(configuration: .default)
        }
        
        
        let task = session.dataTask(with: request) { ( data, response, error ) in
            if(error != nil) {
                return
            }
            
            let responseString = String(NSString(data: data!, encoding: String.Encoding.utf8.rawValue)!)
            let httpResponse = response as? HTTPURLResponse
            let responseCode = httpResponse?.statusCode
            
            var r: Dictionary = Dictionary<String, Any>()
            r["body"] = responseString
            r["code"] = responseCode
            completion(r)
        }
        task.resume()
    }
    
    // Get Card Sets
    public func getCardSets(url: String, method: String, headers:Dictionary<String, String>, dataCertPath: Data, sslOrNot: Bool, completion: @escaping (Dictionary<String, Any>) -> ()) {
        
        let urlPath = "/manage/cardsets/"
        let urlURL = URL(string: url+urlPath)!
        var request = URLRequest(url: urlURL)
        request.httpMethod = method
        request.setValue("application/json", forHTTPHeaderField: "content-type")
        
        headers.forEach {(key: String, value: String) in
            request.setValue(value, forHTTPHeaderField: key)
        }
        
        if(sslOrNot) {
            let portToString = String(urlURL.port!)
            let urlTAS = urlURL.host!+":"+portToString
            session = TasCertificatesManager().getSessionTas(dataCertPath: [dataCertPath], url: urlTAS)
        } else {
            session = URLSession(configuration: .default)
        }
        
        let task = session.dataTask(with: request) { ( data, response, error ) in
            if(error != nil) {
                return
            }
            
            let responseString = String(NSString(data: data!, encoding: String.Encoding.utf8.rawValue)!)
            let httpResponse = response as? HTTPURLResponse
            let responseCode = httpResponse?.statusCode
            
            var r: Dictionary = Dictionary<String, Any>()
            r["body"] = responseString
            r["code"] = responseCode
            completion(r)
        }
        task.resume()
    }
    
    // Refresh Token
    public func refreshToken(url: String, method: String, headers:Dictionary<String, String>, dataCertPath: Data, sslOrNot: Bool, completion: @escaping (Dictionary<String, Any>) -> ()) {
        
        let urlPath = "/refresh/token"
        let urlURL = URL(string: url+urlPath)!
        var request = URLRequest(url: urlURL)
        request.httpMethod = method
        request.setValue("application/json", forHTTPHeaderField: "content-type")
        
        headers.forEach {(key: String, value: String) in
            request.setValue(value, forHTTPHeaderField: key)
        }
        
        if(sslOrNot) {
            let portToString = String(urlURL.port!)
            let urlTAS = urlURL.host!+":"+portToString
            session = TasCertificatesManager().getSessionTas(dataCertPath: [dataCertPath], url: urlTAS)
        } else {
            session = URLSession(configuration: .default)
        }
        
        let task = session.dataTask(with: request) { ( data, response, error ) in
            if(error != nil) {
                return
            }
            
            let responseString = String(NSString(data: data!, encoding: String.Encoding.utf8.rawValue)!)
            let httpResponse = response as? HTTPURLResponse
            let responseCode = httpResponse?.statusCode
            
            var r: Dictionary = Dictionary<String, Any>()
            r["body"] = responseString
            r["code"] = responseCode
            completion(r)
        }
        task.resume()
    }
}

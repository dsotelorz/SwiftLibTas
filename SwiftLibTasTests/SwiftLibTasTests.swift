//
//  SwiftLibTasTests.swift
//  SwiftLibTasTests
//
//  Created by TAS on 10/6/21.
//

import XCTest
@testable import SwiftLibTas

class SwiftLibTasTests: XCTestCase {
    
    var swiftLibTas: SwiftLibTas!

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

    override func setUp() {
        swiftLibTas = SwiftLibTas()
    }

    func testAdd() {
        XCTAssertEqual(swiftLibTas.add(a: 1, b: 1), 2)
    }
    
    func testSub() {
        XCTAssertEqual(swiftLibTas.sub(a: 2, b: 1), 1)
    }
    
    func testURL() {
        /*let urlSession = URLSession.shared
        let url = URL(string: "http://tasiberiacordoba.dyndns.org:8180/TAS-CIS/login")
        
        urlSession.dataTask(with: url!) { data, response, error in
            print("Data \(String(describing: data))")
            print("Response \(String(describing: response))")
            print("Error \(String(describing: error))")
        }.resume()*/
        
        //swiftLibTas.getUserData(url: "https://jsonplaceholder.typicode.com/todos/1")
        //swiftLibTas.getUserData(url: "http://tasiberiacordoba.dyndns.org:8180/TAS-CIS/login")
        
    }

}

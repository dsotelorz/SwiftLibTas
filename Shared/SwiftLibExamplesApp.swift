//
//  SwiftLibExamplesApp.swift
//  Shared
//
//  Created by TAS on 10/6/21.
//

import SwiftUI

@main
struct SwiftLibExamplesApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
